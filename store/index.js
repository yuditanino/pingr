export const state = () => ({
    email: '',
    password: '',
    username: '',
    token: '',
})

export const mutations = {
    addUser(state, user){
        state.email = user.email;
        state.password = user.password;
        state.username = user.username;
    },
    addToken(state, token){
        state.token = token;
    }
}